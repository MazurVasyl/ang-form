import {Component, Input, OnInit} from '@angular/core';
import {us} from '../../../containers/register/register.component';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-first-name',
  templateUrl: './first-name.component.html'
})
export class FirstNameComponent implements OnInit {
  @Input() regUser: us;
  @Input() form: NgForm;

  constructor() { }

  ngOnInit(): void {
  }

}
