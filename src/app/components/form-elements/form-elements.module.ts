import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstNameComponent } from './first-name/first-name.component';
import { LastNameComponent } from './last-name/last-name.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [FirstNameComponent, LastNameComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [FirstNameComponent, LastNameComponent]
})
export class FormElementsModule { }
