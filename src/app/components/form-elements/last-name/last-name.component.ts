import {Component, Input, OnInit} from '@angular/core';
import {us} from '../../../containers/register/register.component';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-last-name',
  templateUrl: './last-name.component.html'
})
export class LastNameComponent implements OnInit {
  @Input() regUser: us;
  @Input() form: NgForm;

  constructor() { }

  ngOnInit(): void {
  }

}
