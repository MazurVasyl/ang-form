import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent } from './register.component';
import {FormElementsModule} from '../../components/form-elements/form-elements.module';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    FormElementsModule,
    FormsModule
  ],
  exports: [RegisterComponent]
})
export class RegisterModule { }
