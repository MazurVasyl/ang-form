import { Component, OnInit } from '@angular/core';

export type us = {
  firstName: string;
  lastName: string;
};

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  user: us = {
    firstName: '',
    lastName: ''
  };

  constructor() { }

  ngOnInit(): void {
  }

  sendForm(values: any): void {
    console.log(values);
  }
}
